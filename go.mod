module code.gitea.io/tea

go 1.13

require (
	code.gitea.io/gitea-vet v0.2.1
	code.gitea.io/sdk/gitea v0.13.1-0.20201209180822-68eec69f472e
	github.com/AlecAivazis/survey/v2 v2.2.2
	github.com/Microsoft/go-winio v0.4.15 // indirect
	github.com/adrg/xdg v0.2.2
	github.com/alecthomas/chroma v0.8.1 // indirect
	github.com/araddon/dateparse v0.0.0-20201001162425-8aadafed4dc4
	github.com/charmbracelet/glamour v0.2.0
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/go-git/go-git/v5 v5.2.0
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/kevinburke/ssh_config v0.0.0-20201106050909-4977a11b4351 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/microcosm-cc/bluemonday v1.0.4 // indirect
	github.com/muesli/reflow v0.2.0 // indirect
	github.com/muesli/termenv v0.7.4
	github.com/olekukonko/tablewriter v0.0.4
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.3.0
	github.com/xanzy/ssh-agent v0.3.0 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
	golang.org/x/sys v0.0.0-20201107080550-4d91cf3a1aaf // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/tools v0.0.0-20201105220310-78b158585360 // indirect
	gopkg.in/yaml.v2 v2.3.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
